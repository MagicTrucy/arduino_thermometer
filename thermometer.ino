#include <LiquidCrystal.h>
#define DEBUG 0

const int sensorPin = A0;
LiquidCrystal lcd(12,11,5,4,3,2);

void setup() {
  if (DEBUG == 1)
    Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Temperature");
}

void loop() {
  int sensorVal = analogRead(sensorPin);
  float tension = (sensorVal/1024.0) * 5.0;
  float temperature = (tension - 0.5) * 100;
  if (DEBUG == 1)
  {
    Serial.print("Capteur :");
    Serial.print(sensorVal);
    Serial.print(", Tension :");
    Serial.print(tension);
    Serial.print(", Temperature :");
    Serial.println(temperature);
  }
  lcd.setCursor(0, 1);
  lcd.print(temperature);
  delay(10000);
}

